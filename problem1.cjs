const fs = require("fs");
const path = require("path");

function jsonFileCreator() {
    fs.mkdir("./JSONfiles", (err, data) => {
        if (err) {
            console.error(err);
        } else {
            let randomNumber = Math.floor(Math.random() * 10);
            function recursive(NumberCount) {
                let randomFilePath = path.join(__dirname, `./JSONfiles/random${NumberCount}.json`);
                if (NumberCount < 0) {
                    return;
                } else {
                    fs.writeFile(randomFilePath, JSON.stringify("Hello"), (err, data) => {
                        if (err) {
                            console.error(err);
                        } else {
                            console.log(`Done creating the file ${path.basename(randomFilePath)}`);
                            fs.unlink(randomFilePath, (err, data) => {
                                if (err) {
                                    console.log(err);
                                } else {
                                    console.log(`Done deleting file ${path.basename(randomFilePath)}`);
                                }
                            });
                        }
                    });
                }
                recursive(--NumberCount);
            }
            recursive(randomNumber);
        }
    });
}

module.exports = jsonFileCreator;
