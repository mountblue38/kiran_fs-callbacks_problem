const fs = require("fs");
const path = require("path");
const textfilepath = path.join(__dirname, "./lipsum.txt");

function problem2() {
    fs.readFile(textfilepath, "utf-8", (err, data) => {
        if (err) {
            console.error(err);
        } else {
            console.log(`started reading the file ....`);
            let fileName = "uppercase.txt";

            fs.writeFile(`./${fileName}`, data.toUpperCase(), (err, data) => {
                if (err) {
                    console.error(err);
                } else {
                    console.log(`started writing uppercase.txt the file`);

                    fs.appendFile("./filesName.txt", fileName + "\n", (err) => {
                        if (err) {
                            console.error(err);
                        } else {
                            console.log(`${fileName} (filename) is pushed to filenames.txt`);

                            fs.readFile("./uppercase.txt", "utf-8", (err, data) => {
                                if (err) {
                                    console.error(err);
                                } else {
                                    console.log(`started reading the uppercase.txt file....`);
                                    console.log(`started spliting into sentence`);
                                    data = data.toLowerCase();
                                    arrOfSentence = data
                                        .split(". ")
                                        .map((sentence) => {
                                            return sentence.charAt(0).toUpperCase() + sentence.slice(1);
                                        })
                                        .join(".\n");
                                    let fileName = "sentence.txt";

                                    fs.writeFile(`./${fileName}`, String(arrOfSentence), (err, data) => {
                                        if (err) {
                                            console.error(err);
                                        } else {
                                            console.log(`started writing the sentence.txt file....`);

                                            fs.appendFile("./filesName.txt", fileName + "\n", (err) => {
                                                if (err) {
                                                    console.error(err);
                                                } else {
                                                    console.log(`${fileName} (filename) is pushed to filenames.txt`);

                                                    fs.readFile("./filesName.txt", "utf-8", (err, data) => {
                                                        if (err) {
                                                            console.error(err);
                                                        } else {
                                                            console.log("started reading the file...");
                                                            data.split("\n")
                                                                .slice(0, -1)
                                                                .map((fileName) => {
                                                                    return fs.unlink(`./${fileName}`, (err, data) => {
                                                                        if (err) {
                                                                            console.error(err);
                                                                        } else {
                                                                            console.log(`Done deleting the file ${fileName}`);
                                                                        }
                                                                    });
                                                                });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

module.exports = problem2;
